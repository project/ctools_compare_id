-------------------------------------------------------------------------------
Ctools Compare ID for Drupal 7.x
-------------------------------------------------------------------------------
Description:
The module provides a ctools plugins to provide access control by checking for equality ID.

Supported entities for check access:
- Node
- User

An example of using the module:
- Deny the display of the page to a particular user, if you know its identifier.
- Overriding a specific page.
